import IOrder from 'interfaces/models/order';
import { IPaginationParams, IPaginationResponse } from 'interfaces/pagination';
import { Observable } from 'rxjs';

import apiService, { ApiService } from './api';

export class OrderService {
  constructor(private apiService: ApiService) {}

  public list(params: IPaginationParams): Observable<IPaginationResponse<IOrder>> {
    return this.apiService.get('/orders', params);
  }

  public save(model: Partial<IOrder>): Observable<IOrder> {
    const order = {
      description: model.description,
      quantity: parseInt(model.quantity.toString()),
      price: model.price
    };

    return this.apiService.post('/orders', order);
  }

  public delete(id: number): Observable<void> {
    return this.apiService.delete(`/orders/${id}`);
  }
}

const orderService = new OrderService(apiService);
export default orderService;
